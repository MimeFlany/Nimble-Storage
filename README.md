PRTG Device Template for Nimble Storage device
==============================================


This project contains all the files necessary to integrate the Nimble Storage
into PRTG for auto discovery and sensor creation.

It will create two kinds of sensors:
  Nimble Total Stats:
          Reads
          Writes
          Seq Reads
          Seq Writes
          ReadTime
          Write Time
          Read Bytes
          Write Bytes
          Read Seq Bytes
          Write Seq Bytes
 
 For each volume publisend:
  Vol: {Volume Name}
          Online
          Connections
          IO Reads
          IO Write
          Avg Read
          Avg Write
          Read Non-Seq Mem Hits
          Read Non-Seq SSD Hits
          Read Latency 500ms+
          Write Latency 500ms+

Download Instructions
=========================
[A zip file containing all the files in the project can be downloaded from the 
repository](https://gitlab.com/PRTG-Projects/Device-Templates/Nimble-Storage/-/jobs/artifacts/master/download?job=PRTGDistZip) 


Installation Instructions
=========================
Please refer to INSTALL.md
